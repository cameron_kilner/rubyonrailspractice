module ApplicationHelper
	def hidden_div_if(condition,attributes={},&block)
		if condition
			attributes["style"]="display:none"
		end
		content_tag("div",attributes,&block)
	end

	def us_to_euro(amt)
		if I18n.locale.to_s=='eu'
			return amt*0.93
		else
			amt
		end
	end
end