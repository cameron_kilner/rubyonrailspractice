class StoreController < ApplicationController
	include CurrentCart
  skip_before_action :authorize
	before_action :set_cart
  def index
    if params[:set_locale]
      redirect_to store_url(locale: params[:set_locale])
    else
    	@products=Product.order(:title).where("locale = '#{I18n.locale}'")
    	@visits=track_visits
    end
  end

  def track_visits
  	session[:visits]||=0
  	session[:visits]+=1
  end
end