class UpdateLineItemsPrice < ActiveRecord::Migration
  	def up
		LineItem.all.each do |item|
			item.price=Product.find(item.product_id).price
			#could be item.product.price also
			item.save!
		end
	end
	def down
		LineItem.all.each do |item|
			item.price=100000
			item.save!
		end
	end
end
