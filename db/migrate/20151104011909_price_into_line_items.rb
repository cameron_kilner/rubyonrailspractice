class PriceIntoLineItems < ActiveRecord::Migration
	def change
		add_column :line_items, :price, :float, default: 1000000
	end
end
